
% ************************************************************************************************************************************************************ %
%			% ------ Implementation of Implicit Time Integrations Schemes for Linear, Non-Linear and Analytical methods for dynamic problem ------ %		   %
% ************************************************************************************************************************************************************ %

% | --- Written by Sumanth Bharadwaj

function varargout = PPP_61496_Time_Intergration_Schemes(varargin)
% PPP_61496_TIME_INTERGRATION_SCHEMES MATLAB code for PPP_61496_Time_Intergration_Schemes.fig
%      PPP_61496_TIME_INTERGRATION_SCHEMES, by itself, creates a new PPP_61496_TIME_INTERGRATION_SCHEMES or raises the existing
%      singleton*.
%
%      H = PPP_61496_TIME_INTERGRATION_SCHEMES returns the handle to a new PPP_61496_TIME_INTERGRATION_SCHEMES or the handle to
%      the existing singleton*.
%
%      PPP_61496_TIME_INTERGRATION_SCHEMES('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in PPP_61496_TIME_INTERGRATION_SCHEMES.M with the given input arguments.
%
%      PPP_61496_TIME_INTERGRATION_SCHEMES('Property','Value',...) creates a new PPP_61496_TIME_INTERGRATION_SCHEMES or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before PPP_61496_Time_Intergration_Schemes_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to PPP_61496_Time_Intergration_Schemes_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help PPP_61496_Time_Intergration_Schemes

% Last Modified by GUIDE v2.5 14-Apr-2019 12:30:35

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @PPP_61496_Time_Intergration_Schemes_OpeningFcn, ...
                   'gui_OutputFcn',  @PPP_61496_Time_Intergration_Schemes_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before PPP_61496_Time_Intergration_Schemes is made visible.
function PPP_61496_Time_Intergration_Schemes_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to PPP_61496_Time_Intergration_Schemes (see VARARGIN)

% Choose default command line output for PPP_61496_Time_Intergration_Schemes
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes PPP_61496_Time_Intergration_Schemes wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = PPP_61496_Time_Intergration_Schemes_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


function edit2_Callback(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit2 as text
%        str2double(get(hObject,'String')) returns contents of edit2 as a double


% --- Executes during object creation, after setting all properties.
function edit2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popupmenu1.
function popupmenu1_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu1


% --- Executes during object creation, after setting all properties.
function popupmenu1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popupmenu2.
function popupmenu2_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu2 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu2


% --- Executes during object creation, after setting all properties.
function popupmenu2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popupmenu3.
function popupmenu3_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu3 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu3


% --- Executes during object creation, after setting all properties.
function popupmenu3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popupmenu4.
function popupmenu4_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu4 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu4


% --- Executes during object creation, after setting all properties.
function popupmenu4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popupmenu5.
function popupmenu5_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu5 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu5


% --- Executes during object creation, after setting all properties.
function popupmenu5_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% ------------- Initialization of Geometry and Material Parameters ------------- %

B1 = 1.0;                           % [m]
B2 = 0.1;                           % [m]
H = 0.7;                            % Height in [m]
EA = 2*10^7;                        % [N] 
roA = 1.0;                          % [kg/m]                          
alpha = 100;                        % Dimensionless
beta = 100; 		                % Dimensionless
NEQ = 2;							% 2D case
L1 = sqrt(B1^2 + H^2);              % Length_1 of element in [m]
L2 = sqrt(B2^2 + H^2);              % Length_2 of element in [m]
r = (EA*H^3)/(3*sqrt(3)*L1^3);      % Load in [N] 
alpha_1 = 100;                      % RAYLEIGH Damping Parameter in [1/s]
alpha_2 = 0;                        % RAYLEIGH Damping Parameter in [s]

% ************************************************************************************************************************************************** %

contents_spectralradius = cellstr(get(handles.popupmenu2,'String'));
selected_spectralradius = str2double(contents_spectralradius{get(handles.popupmenu2,'Value')});
ro_inf = selected_spectralradius;
selected_scheme = get(handles.popupmenu1,'Value');
SCHEME = selected_scheme;


% ------------- Initializing the Parameters  ------------- %
switch SCHEME
    case 1
        schemeName = 'Newmark';
        alpha_m = 0;
        alpha_f = 0;
        beta_s = 1/(ro_inf + 1)^2;
        gamma_s = (3 - ro_inf)/(2*ro_inf + 2);
        
    case 2
        schemeName = 'Hilber-\alpha';
        alpha_m = 0;
        alpha_f = (1 - ro_inf)/(ro_inf + 1);
        beta_s = 0.25*(1 + alpha_f)^2;
        gamma_s = 0.5 + alpha_f;
        
    case 3
        schemeName = 'Bossak-\alpha';
        alpha_m = (ro_inf - 1)/(ro_inf + 1);
        alpha_f = 0;
        beta_s = 0.25*(1 - alpha_m)^2;
        gamma_s = 0.5 - alpha_m;
        
    case 4
        schemeName = 'Newmark-\alpha';
        alpha_m = (2*ro_inf - 1)/(ro_inf + 1);
        alpha_f = ro_inf/(ro_inf + 1);
        beta_s = 0.25*(1 - alpha_m + alpha_f)^2;
        gamma_s = 0.5 - alpha_m + alpha_f;
end

contents_lambda = cellstr(get(handles.popupmenu4,'String'));
selected_lambda = str2double(contents_lambda{get(handles.popupmenu4,'Value')});
lamda = selected_lambda;
contents_step = cellstr(get(handles.popupmenu5,'String'));
selected_step = str2double(contents_step{get(handles.popupmenu5,'Value')});
dt = (10^-3)*selected_step;


% ************************************************************************************************************************************************** %
									% ************************************************************************** %
									% ------------ 1. Non - Linear Solution for the Dynamic Problem ------------ %
									% ************************************************************************** %
% ************************************************************************************************************************************************** %

t = (10^-3)*100;
NT = round(t/dt);
time = 0:dt:dt*NT;
U = zeros(2,NT+1);					% Initialization for Displacement
U_dot = zeros(2,NT+1);				% Initialization for Velocity
U_dot_dot = zeros(2,NT+1);			% initialization for Acceleration

% ------------- The Mass, Damping and Stiffness Matrices ------------- %
M = (roA/3)*[ L1+beta*L2, 0; 0, L1];								   % Mass Matrix
K = (EA/L1^3)*[ B1^2+((alpha*L1^3)/L2^3)*B2^2, -B1*H; -B1*H, H^2];	   % Stiffness Matrix
D = alpha_1*M + alpha_2*K;											   % Damping Matrix

R = lamda*[0; -r];													   % Load

% ------------- Tangent Stiffness Matrix Kt ------------- %
 syms u1 u2
 Ri = (EA/2)*[(-B1+u1)*((-2*B1+u1)*u1 + (2*H+u2)*u2)/L1^3 + alpha*(B2+u1)*(2*B2+u1)*u1/L2^3; (H+u2)*((-2*B1+u1)*u1 + (2*H+u2)*u2)/L1^3];
 Kt = [diff(Ri(1),u1), diff(Ri(1),u2) ; diff(Ri(2),u1), diff(Ri(2),u2)];

% ------------- Calculating static deformation for initializing U_0 ------------- %

    % ------- Initial Conditions ------- %
    U_0 = [0; 0];
    lamda_0 = 0;

    LoadSteps = 1;        							% Load Steps
    Lamda = lamda_0:(lamda/LoadSteps):lamda;
    U_NL_num = zeros(2,LoadSteps+1);

    U_n = U_0;
    lamda_n = lamda_0;
    U_NL_num(:,1) = U_n;

for n=0:1:LoadSteps-1    
    
    % ------- Newton-Raphson Iterations ------- %
    
    k=0;
    lamda_np1 = Lamda(n+1+(1));
    R = lamda_np1*[0; -r];
    
    U_np1_0 = U_n;
    Ri_np1_0 = lamda_n*[0; -r];
    u1 = U_np1_0(1);
    u2 = U_np1_0(2);
    Kt_np1_0 = (EA/2)*[(2*(B1-u1)^2 - (2*B1-u1)*u1 + (2*H+u2)*u2)/L1^3 + alpha*(2*(B2+u1)^2 + (2*B2+u1)*u1)/L2^3, -2*(B1-u1)*(H+u2)/L1^3;
                                                            -2*(B1-u1)*(H+u2)/L1^3                                  , (2*(H+u2)^2 - (2*B1-u1)*u1 + (2*H+u2)*u2)/L1^3];
    DU = Kt_np1_0\((lamda_np1 - lamda_n)*[0; -r]); 				%vpa(DU)
    U_np1_1 = U_np1_0 + DU;
    error = norm(DU)/norm(U_np1_1 - U_n); 						% Displacement based relative error
    k=k+1;
    
    % k=1 onwards
    U_np1_k = U_np1_1;
    
    while(error >= 10^-6)
        u1 = U_np1_k(1);
        u2 = U_np1_k(2);
        Ri_np1_k = (EA/2)*[(-B1+u1)*((-2*B1+u1)*u1 + (2*H+u2)*u2)/L1^3 + alpha*(B2+u1)*(2*B2+u1)*u1/L2^3;
                                                (H+u2)*((-2*B1+u1)*u1 + (2*H+u2)*u2)/L1^3];
        
        Kt_np1_k = (EA/2)*[(2*(B1-u1)^2 - (2*B1-u1)*u1 + (2*H+u2)*u2)/L1^3 + alpha*(2*(B2+u1)^2 + (2*B2+u1)*u1)/L2^3, -2*(B1-u1)*(H+u2)/L1^3;
                                                            -2*(B1-u1)*(H+u2)/L1^3                                  , (2*(H+u2)^2 - (2*B1-u1)*u1 + (2*H+u2)*u2)/L1^3];
                
        DU = Kt_np1_k\(lamda_np1*[0; -r] - Ri_np1_k); 			%vpa(lamda_np1*[0; -r] - Ri_np1_k) %vpa(DU)
        U_np1_kp1 = U_np1_k + DU;
        error = norm(DU)/norm(U_np1_kp1 - U_n); 				% Displacement based relative error
        U_np1_k = U_np1_kp1;
        k=k+1;
    end
    
    U_NL_num(:,n+1+(1)) = U_np1_k; 								% OR U_np1_kp1 => will store the SAME RESULT!!!
    %disp(['Load Stage = ' num2str(n+1) '/' num2str(LoadSteps) ' Newton Raphson Iterations = ' num2str(k-1)]);
    
    U_n = U_np1_k;
    lamda_n = lamda_np1;    
end


% ------------- Numerical Solution, n=0 [Initial Conditions] ------------- %
U_0 = U_NL_num(:,end);
U_dot_0 = [0; 0];
U_dot_dot_0 = -M\(K*U_0 + D*U_dot_0);
R_0 = [0; 0];

% ------------- Storing Numerical Solution, n=0 [Initial Conditions] ------------- %
U(:,1) = U_0;
U_dot(:,1) = U_dot_0;
U_dot_dot(:,1) = U_dot_dot_0;

% ------------- Numerical Solution from n=1 to n=NT ------------- %
U_n = U_0;
U_dot_n = U_dot_0;
U_dot_dot_n = U_dot_dot_0;
flag = false;                      								% Flag to indicate if solution is not convergent

for n=0:1:NT-1
    
    % ------- Numerical Solution ------- %
    R_n = R_0; 													% If load is dynamic, corresponding value should be assigned instead of R_0
    R_np1 = R_0; 												
    R_np1_alphaf = (1-alpha_f)*R_np1 + (alpha_f)*R_n;
    
    % Loop of k
    k=0; 
    U_np1_k = U_n;
    U_dot_np1_k = (gamma_s/(beta_s*dt))*(U_np1_k - U_n) - ((gamma_s-beta_s)/beta_s)*U_dot_n - ((gamma_s-2*beta_s)/(2*beta_s))*dt*U_dot_dot_n;
    U_dot_dot_np1_k = (1/(beta_s*dt^2))*(U_np1_k - U_n) - (1/(beta_s*dt))*U_dot_n - ((1-2*beta_s)/(2*beta_s))*U_dot_dot_n;
    
    U_np1_alphaf_k = (1-alpha_f)*U_np1_k + (alpha_f)*U_n;
    U_dot_np1_alphaf_k = (1-alpha_f)*U_dot_np1_k + (alpha_f)*U_dot_n;
    U_dot_dot_np1_alpham_k = (1-alpha_m)*U_dot_dot_np1_k + (alpha_m)*U_dot_dot_n;
    
    u1 = U_np1_alphaf_k(1);
    u2 = U_np1_alphaf_k(2);
    Ri_np1_alphaf_k = (EA/2)*[(-B1+u1)*((-2*B1+u1)*u1 + (2*H+u2)*u2)/L1^3 + alpha*(B2+u1)*(2*B2+u1)*u1/L2^3;
                                                                  (H+u2)*((-2*B1+u1)*u1 + (2*H+u2)*u2)/L1^3];
    
    Kt_np1_alphaf_k = (EA/2)*[(2*(B1-u1)^2 - (2*B1-u1)*u1 + (2*H+u2)*u2)/L1^3 + alpha*(2*(B2+u1)^2 + (2*B2+u1)*u1)/L2^3,                         -2*(B1-u1)*(H+u2)/L1^3;
                                                               -2*(B1-u1)*(H+u2)/L1^3                                  , (2*(H+u2)^2 - (2*B1-u1)*u1 + (2*H+u2)*u2)/L1^3];

    Reff = R_np1_alphaf - Ri_np1_alphaf_k - D*U_dot_np1_alphaf_k - M*U_dot_dot_np1_alpham_k;			% Effective right hand side
    
    Kteff = M*(1-alpha_m)/(beta_s*dt^2) + D*gamma_s*(1-alpha_f)/(beta_s*dt) + Kt_np1_alphaf_k*(1-alpha_f);
    
    DU = Kteff\Reff;
    U_np1_kp1 = U_np1_k + DU;
    error = norm(DU)/norm(U_np1_kp1 - U_n); 					% Displacement based relative error
    k=k+1;
    
    % k=1 onwards
    U_np1_k = U_np1_kp1;
    
    while(error >= 10^-6)
        U_dot_np1_k = (gamma_s/(beta_s*dt))*(U_np1_k - U_n) - ((gamma_s-beta_s)/beta_s)*U_dot_n - ((gamma_s-2*beta_s)/(2*beta_s))*dt*U_dot_dot_n;
        U_dot_dot_np1_k = (1/(beta_s*dt^2))*(U_np1_k - U_n) - (1/(beta_s*dt))*U_dot_n - ((1-2*beta_s)/(2*beta_s))*U_dot_dot_n;
        
        U_np1_alphaf_k = (1-alpha_f)*U_np1_k + (alpha_f)*U_n;
        U_dot_np1_alphaf_k = (1-alpha_f)*U_dot_np1_k + (alpha_f)*U_dot_n;
        U_dot_dot_np1_alpham_k = (1-alpha_m)*U_dot_dot_np1_k + (alpha_m)*U_dot_dot_n;
        
        u1 = U_np1_alphaf_k(1);
        u2 = U_np1_alphaf_k(2);
        Ri_np1_alphaf_k = (EA/2)*[(-B1+u1)*((-2*B1+u1)*u1 + (2*H+u2)*u2)/L1^3 + alpha*(B2+u1)*(2*B2+u1)*u1/L2^3;
            (H+u2)*((-2*B1+u1)*u1 + (2*H+u2)*u2)/L1^3];
        
        Kt_np1_alphaf_k = (EA/2)*[(2*(B1-u1)^2 - (2*B1-u1)*u1 + (2*H+u2)*u2)/L1^3 + alpha*(2*(B2+u1)^2 + (2*B2+u1)*u1)/L2^3, -2*(B1-u1)*(H+u2)/L1^3;
            -2*(B1-u1)*(H+u2)/L1^3                                  , (2*(H+u2)^2 - (2*B1-u1)*u1 + (2*H+u2)*u2)/L1^3];
        
        Reff = R_np1_alphaf - Ri_np1_alphaf_k - D*U_dot_np1_alphaf_k - M*U_dot_dot_np1_alpham_k; %vpa(Reff)
        
        Kteff = M*(1-alpha_m)/(beta_s*dt^2) + D*gamma_s*(1-alpha_f)/(beta_s*dt) + Kt_np1_alphaf_k*(1-alpha_f); %vpa(Kteff)
        
        DU = Kteff\Reff; %vpa(DU)
        U_np1_kp1 = U_np1_k + DU;
        error = norm(DU)/norm(U_np1_kp1 - U_n); 				%disp(['error = ' num2str(error)])
        k=k+1;
        U_np1_k = U_np1_kp1;
        if k > 500
            disp(['Status : No convergence at n = ' num2str(n) '  !!! Hence terminating the program !!!']);
            flag = 1;
            break;
        end
            
    end
    
    if flag
        break;
    end
    
    %disp(['Time Step = ' num2str(n+1) '/' num2str(NT) ' Newton Raphson Iterations = ' num2str(k-1)]);
    
    U_np1 = U_np1_k;
    U_dot_np1 = (gamma_s/(beta_s*dt))*(U_np1 - U_n) - ((gamma_s-beta_s)/beta_s)*U_dot_n - ((gamma_s-2*beta_s)/(2*beta_s))*dt*U_dot_dot_n;
    U_dot_dot_np1 = (1/(beta_s*dt^2))*(U_np1 - U_n) - (1/(beta_s*dt))*U_dot_n - ((1-2*beta_s)/(2*beta_s))*U_dot_dot_n;
    
	%%%%%%%%%%%%%%%%%%%%%%%%%%% Non-Linear Solution %%%%%%%%%%%%%%%%%%%%%%%%%%%
    % ------------- Storing Numerical Solution : additional +(1) must be added to indices because MATLAB array index starts with 1 (NOT 0) ------------- %
    U(:,n+1+(1)) = U_np1;
    U_dot(:,n+1+(1)) = U_dot_np1;
    U_dot_dot(:,n+1+(1)) = U_dot_dot_np1;
    
    % ------------- Updating Variables ------------- %
    U_n = U_np1;
    U_dot_n = U_dot_np1;
    U_dot_dot_n = U_dot_dot_np1;
    
end


% ************************************************************************************************************************************************** %
									% ************************************************************************** %
									% --------------- 2. Linear Solution for the Dynamic Problem --------------- %
									% ************************************************************************** %
% ************************************************************************************************************************************************** %

U_lin = zeros(2,NT+1);				% Initialization for Displacement
U_dot_lin = zeros(2,NT+1);			% Initialization for Velocity
U_dot_dot_lin = zeros(2,NT+1);		% Initialization for Acceleration

% ------------- Numerical Solution, n=0 [Initial Conditions] ------------- %
U_0 = K\R;
U_dot_0 = [0; 0];
U_dot_dot_0 = -M\(K*U_0 + D*U_dot_0);
R_0 = [0; 0];

Keff = M*(1-alpha_m)/(beta_s*dt^2) + D*gamma_s*(1-alpha_f)/(beta_s*dt) + K*(1-alpha_f);

% ------------- Storing Numerical Solution, n=0 [Initial Conditions] ------------- %
U_lin(:,1) = U_0;
U_dot_lin(:,1) = U_dot_0;
U_dot_dot_lin(:,1) = U_dot_dot_0;

% ------------- Numerical Solution from n=1 to n=NT ------------- %
U_n = U_0;
U_dot_n = U_dot_0;
U_dot_dot_n = U_dot_dot_0;
U_np1 = [0, 0];

for n=0:1:NT-1
    
    % ------- Numerical Solution ------- %
    R_n = R_0; 														% If load is dynamic, corresponding value should be assigned instead of R_0
    R_np1 = R_0; 													% If load is dynamic, corresponding value should be assigned instead of R_0
    R_np1_alphaf = (1-alpha_f)*R_np1 + (alpha_f)*R_n;
    
    % ------ Keff = same since NO ADAPTIVE time steps ------- %
    Reff = R_np1_alphaf - K*(alpha_f*U_n) + D*((gamma_s*(1-alpha_f)/(beta_s*dt))*U_n + ((gamma_s-gamma_s*alpha_f-beta_s)/beta_s)*U_dot_n + ((gamma_s-2*beta_s)*(1-alpha_f)*dt/(2*beta_s))*U_dot_dot_n) + M*(((1-alpha_m)/(beta_s*dt^2))*U_n + ((1-alpha_m)/(beta_s*dt))*U_dot_n + ((1-alpha_m-2*beta_s)/(2*beta_s))*U_dot_dot_n);
    U_np1 = Keff\Reff;
    U_dot_np1 = (gamma_s/(beta_s*dt))*(U_np1 - U_n) - ((gamma_s-beta_s)/beta_s)*U_dot_n - ((gamma_s-2*beta_s)/(2*beta_s))*dt*U_dot_dot_n;
    U_dot_dot_np1 = (1/(beta_s*dt^2))*(U_np1 - U_n) - (1/(beta_s*dt))*U_dot_n - ((1-2*beta_s)/(2*beta_s))*U_dot_dot_n;
    
	%%%%%%%%%%%%%%%%%%%%%%%%%%% Linear Solution %%%%%%%%%%%%%%%%%%%%%%%%%%%
    % ------- Storing Numerical Solution : additional +(1) must be added to indices because MATLAB array index starts with 1 (NOT 0) ------- %
    U_lin(:,n+1+(1)) = U_np1;
    U_dot_lin(:,n+1+(1)) = U_dot_np1;
    U_dot_dot_lin(:,n+1+(1)) = U_dot_dot_np1;
    
    % ------- Updating Variables ------- %
    U_n = U_np1;
    U_dot_n = U_dot_np1;
    U_dot_dot_n = U_dot_dot_np1;
    
end

selected_variable = get(handles.popupmenu6,'Value')


% ************************************************************************************************************************************************** %
									% ************************************************************************** %
									% ------------- 3. Analytical Solution for the Dynamic Problem ------------- %
									% ************************************************************************** %
% ************************************************************************************************************************************************** %

a = roA^2*(L1+beta*L2)*L1/9;
b = -(EA*roA/3)*((B1^2/L1^3 + alpha*B2^2/L2^3)*L1 + (L1 + beta*L2)*H^2/L1^3);
c = alpha*(EA*B2*H)^2/(L1*L2)^3;

% ------------- Solution of the characteristic polynomial equation for Eigen Value Analysis ------------- %
w1 = sqrt((-b-sqrt(b^2-4*a*c))/(2*a)); 
w2 = sqrt((-b+sqrt(b^2-4*a*c))/(2*a)); 

% ------------- Oscillation Time period ------------- %
T1 = 2*pi/w1;
T2 = 2*pi/w2;

% ------------- Eigen Vector for the Analysis ------------- %
phi_1 = [( L2^3*(H^2*(L1 + beta*L2) - B1^2*L1) - alpha*B2^2*L1^4 + sqrt((L2^3*(H^2*(L1 + beta*L2) + B1^2*L1) + alpha*B2^2*L1^4)^2 - 4*alpha*H^2*B2^2*L1^4*L2^3*(L1 + beta*L2)) )/(2*B1*H*L2^3*(L1 + beta*L2)); 1];
phi_2 = [( L2^3*(H^2*(L1 + beta*L2) - B1^2*L1) - alpha*B2^2*L1^4 - sqrt((L2^3*(H^2*(L1 + beta*L2) + B1^2*L1) + alpha*B2^2*L1^4)^2 - 4*alpha*H^2*B2^2*L1^4*L2^3*(L1 + beta*L2)) )/(2*B1*H*L2^3*(L1 + beta*L2)); 1];

% ------------- Normalizing ------------- %
phi_1 = [phi_1(1)/norm(phi_1); phi_1(2)/norm(phi_1)];
phi_2 = [phi_2(1)/norm(phi_2); phi_2(2)/norm(phi_2)];

% ------------- Modal Matrix ------------- %
PHI = [phi_1, phi_2];

% ------------- Modal Transformed Matrices ------------- %
M_diag = PHI'*M*PHI;
K_diag = PHI'*K*PHI;
D_diag = PHI'*D*PHI;

U_bar_0 = PHI\U_0;
U_bar_dot_0 = PHI\U_dot_0;

%syms t;
U_bar_t = sym('t', [NEQ 1]);
U_bar_dot_t = sym('t', [NEQ 1]);
U_bar_dot_dot_t = sym('t', [NEQ 1]);

for k=1:1:NEQ
    
    u0 = U_bar_0(k);
    v0 = U_bar_dot_0(k);
    
	% ------------- Eigen Angular Frequencies ------------- %
    w = sqrt(K_diag(k,k)/M_diag(k,k));  		% omega
    z = D_diag(k,k)/(2*w*M_diag(k,k));  		% zeta
    wd = w*sqrt(1-z^2);                 		% omega_d
    
	% ------------- Analytical solution using general initial conditions ------------- %
    U_bar_t(k) = exp(-z*w*t)*(u0*cos(wd*t) + ((v0+z*w*u0)/wd)*sin(wd*t));
    U_bar_dot_t(k) = exp(-z*w*t)*(v0*cos(wd*t) - ((z*w*v0+w^2*u0)/wd)*sin(wd*t));
    U_bar_dot_dot_t(k) = exp(-z*w*t)*(((w^2*(2*z^2-1)*v0 + z*w^3*u0)/wd)*sin(wd*t) - (2*z*w*v0+w^2*u0)*cos(wd*t));
    
end

%%%%%%%%%%%%%%%%%%%%%%%%%%% Analytical Solution %%%%%%%%%%%%%%%%%%%%%%%%%%%
U_t = PHI*U_bar_t;
U_dot_t = PHI*U_bar_dot_t;
U_dot_dot_t = PHI*U_bar_dot_dot_t;


% ******************************************************** %
%%%%%%%%%%%%%%%%%%%%% Plotting Results %%%%%%%%%%%%%%%%%%%%%
% ******************************************************** %


switch selected_variable
    case 1
        axes(handles.axes1)
		h=ezplot(U_t(1)/B2, [0,t]); hold on;														% Analytical solution
		set(h,'LineWidth',3);
        plot(time, U_lin(1,:)/B2,'Linewidth',3.0); hold on;											% Linear solution
        plot(time, U(1,:)/B2,'Linewidth',3.0); hold on;												% Non-Linear solution
		grid on
        a=legend('Analytical', 'Linear', 'Non-linear');
        a.FontSize = 12;
        title('Displacements along U_1');
        xlabel('time [s]');
        ylabel({'$ \frac{{u}_{1}}{B_2} $'},'Interpreter','latex', 'Rotation',0, 'fontsize',16);
		xlim([0 0.1])
		ylim([-0.3 0.3])
        
    case 2
        axes(handles.axes1)
		h=ezplot(U_dot_t(1)*T1/B2, [0, t]); hold on;												% Analytical solution
		set(h,'LineWidth',3);
        plot(time, U_dot_lin(1,:)*T1/B2,'Linewidth',3.0); hold on;									% Linear solution
        plot(time, U_dot(1,:)*T1/B2,'Linewidth',3.0); hold on;										% Non-Linear solution
		grid on
        a=legend('Analytical', 'Linear', 'Non-linear');
        a.FontSize = 12;
        title('Velocities along U_1');
        xlabel('time [s]');
        ylabel({'$ \frac{\dot{u}_{1}{T_1}}{B_2} $'},'Interpreter','latex', 'Rotation',0, 'fontsize',16);
		ylim([-2 1.5])
        
    case 3
        axes(handles.axes1)
		h=ezplot(U_dot_dot_t(1)*T1^2/(15*B2), [0, t]); hold on;										% Analytical solution
		set(h,'LineWidth',3);
        plot(time, U_dot_dot_lin(1,:)*T1^2/(15*B2),'Linewidth',3.0); hold on;						% Linear solution
        plot(time, U_dot_dot(1,:)*T1^2/(15*B2),'Linewidth',3.0); hold on;							% Non-Linear solution
		grid on
        a=legend('Analytical', 'Linear', 'Non-linear');
        a.FontSize = 12;
        title('Accelerations along U_1');
        xlabel('time [s]');
        ylabel({'$ \frac{\ddot{u}_{1}{T_1^2}}{15B_2} $'},'Interpreter','latex', 'Rotation',0, 'fontsize',16);
        
    case 4
        h=ezplot(U_t(2)/H, [0,t]); hold on; 														% Analytical solution
		set(h,'LineWidth',3);
        plot(time, U_lin(2,:)/H,'Linewidth',3.0); hold on;											% Linear solution
        plot(time, U(2,:)/H,'Linewidth',3.0); hold on;												% Non-Linear solution
		grid on
        a=legend('Analytical', 'Linear', 'Non-linear');
        a.FontSize = 12;
		title('Displacements along U_2');
        xlabel('time [s]');
        ylabel({'$ \frac{{u}_{2}}{H} $'},'Interpreter','latex', 'Rotation',0, 'fontsize',16);
        
    case 5
        h=ezplot(U_dot_t(2)*T2/H, [0, t]); hold on;													% Analytical solution
		set(h,'LineWidth',3);
		plot(time, U_dot_lin(2,:)*T2/H,'Linewidth',3.0); hold on;									% Linear solution
        plot(time, U_dot(2,:)*T2/H,'Linewidth',3.0); hold on;										% Non-Linear solution
		grid on
        a=legend('Analytical', 'Linear', 'Non-linear');
        a.FontSize = 12;
		title('Velocities along U_2');
        xlabel('time [s]');
        ylabel({'$ \frac{\dot{u}_{2}{T_2}}{H} $'},'Interpreter','latex', 'Rotation',0, 'fontsize',16);
        
    case 6
        h=ezplot(U_dot_dot_t(2)*T2^2/(15*H), [0, t]); hold on; 										% Analytical solution
		set(h,'LineWidth',3);
		plot(time, U_dot_dot_lin(2,:)*T2^2/(15*H),'Linewidth',3.0); hold on;						% Linear solution
        plot(time, U_dot_dot(2,:)*T2^2/(15*H),'Linewidth',3.0); hold on;							% Non-Linear solution
		grid on
        a=legend('Analytical', 'Linear', 'Non-linear');
        a.FontSize = 12;
		title('Accelerations along U_2');
        xlabel('time [s]');
        ylabel({'$ \frac{\ddot{u}_{2}{T_2^2}}{15H} $'},'Interpreter','latex', 'Rotation',0, 'fontsize',16);
                     
end

% --- Executes on button press in pushbutton2.
function pushbutton2_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
cla(handles.axes1,'reset');


% --- Executes on selection change in popupmenu6.
function popupmenu6_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu6 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu6


% --- Executes during object creation, after setting all properties.
function popupmenu6_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes during object creation, after setting all properties.
function uipanel2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to uipanel2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --- Executes on button press in pushbutton3.
function pushbutton3_Callback(hObject, eventdata, handles)
winopen('Help!.pdf')
% hObject    handle to pushbutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes during object creation, after setting all properties.
function pushbutton3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to pushbutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --- Executes on button press in pushExit.
function pushExit_Callback(hObject, eventdata, handles)

close all;

% hObject    handle to pushExit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
