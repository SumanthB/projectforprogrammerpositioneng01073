// ****************************************************************** //
// ********************  GAME OF LIFE IN 3D  ************************ //
// *****************     (Hybrid MPI/OpenMP)    ********************* //
// ****************************************************************** //

#include "HPC_cell.hpp" 

int main ()
{
	int no_of_iterations = 0;
	int intensity = 0.5;
    int size, rank;
    srand (time (NULL)); 
    Cell grid_obj(504, 500, 500, 5, 7, 6, 6);  // Axes dimensions and Life-5766 rules

    // MPI initialization for communication
    MPI_Init (NULL, NULL);
    MPI_Comm_size (MPI_COMM_WORLD, &size);
    MPI_Comm_rank (MPI_COMM_WORLD, &rank);

    // MPI Wall time clause for start time 
    double time_start = MPI_Wtime();
    
	// Grid creation per MPI process
    grid_obj.create_grid (size, rank, intensity); 
	// MPI clauses initialization
    MPI_Barrier (MPI_COMM_WORLD);
    MPI_Allgather (grid_obj.grid_n1, X/size*Y*Z, MPI_INT, grid_obj.grid_n0, X/size*Y*Z, MPI_INT, MPI_COMM_WORLD);
    MPI_Reduce (&grid_obj.local_alive_cells, &grid_obj.global_alive_cells, 1, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);
    MPI_Barrier (MPI_COMM_WORLD);

    double time_grid = MPI_Wtime(); 

	while(no_of_iterations != 200)
    {   
        // Updates the grid per MPI process 
		grid_obj.update_grid (size, rank);
		MPI_Barrier (MPI_COMM_WORLD);
		MPI_Allgather (grid_obj.grid_n1, X/size*Y*Z, MPI_INT, grid_obj.grid_n0, X/size*Y*Z, MPI_INT, MPI_COMM_WORLD);
		MPI_Reduce (&grid_obj.local_alive_cells, &grid_obj.global_alive_cells, 1, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);
		MPI_Barrier (MPI_COMM_WORLD);
		no_of_iterations++;
	}

    double time_simulate = MPI_Wtime(); 

    if (rank == 0) 
		{
			std::cout << "Time spent on grid creation- " << time_grid - time_start << std::endl;  
			std::cout << "Time spent to run for the simulation- " << time_simulate - time_grid << std::endl;  
			std::cout << "The size of MPI- " << size << std::endl;
		}

    MPI_Finalize ();    
    return 0;
}

// ***** End of file *****//