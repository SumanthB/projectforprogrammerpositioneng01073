// ------------ Class cell file -------------- //

#include <iostream>
#include <ctime>
#include "mpi.h"
#include <omp.h>

class Cell
{
	private:
    int x, y, z; 				// Three axes      
    int El, Eu, Fl, Fu; 		// Game of life rules    
	
	public:
    int *grid_n1,               // Stores grid per MPI process
        *grid_n0;               // Stores complete grid
    int local_alive_cells=0,    // Alive cells per MPI process
        global_alive_cells=0;   // Total number of alive cells
        

    /**
	 Constructor
	*/
    Cell (int x, int y, int z, int El, int Eu, int Fl, int Fu) 
	{
		this-> x = x;
		this-> y = y;
		this-> z = z;
		this->grid_n0 = new int[x*y*z];
	}


    ~Cell ()
	{	
	}	

	/**
	-------- Function_1 in class which creates the grid -------- 
	*/
    void create_grid (int size, int rank, double intensity)
	{
		local_alive_cells = 0;
		// Allocates memory for the grid per MPI process 
		this->grid_n1 = new int[x/size*y*z];    
    
        // Open Multiprocessing parallel for implementation 
		#pragma omp parallel for   				
		for (unsigned int i=0; i<x/size; ++i)
		{
			int id = omp_get_thread_num();
			for (unsigned int j=0; j<y; ++j)
			{
				for (unsigned int k=0; k<z; ++k)
				{
					this->grid_n1[i*y*z + j*z + k] = 0;
				}
			}
      
		}

		int i, j, k;        
	
		while (true)
		{
			// pseudo-random value of i, j, k point to a cell which is assigned to 1 
			i = rand () % int(x/size);
			j = rand () % y;
			k = rand () % z;
			if (this->grid_n1 [i*y*z + j*z + k] != 1)
				{
					this->grid_n1 [i*y*z + j*z + k] = 1;
					++local_alive_cells;
				}
			if (local_alive_cells/double(x/size*y*z) >= intensity)
				{
					break;
				}
		}
	}

	/**
	 -------- Function_2 in class which updates the grid --------
	*/
    void update_grid (int size, int rank)
	{
		int count = 0;          
		local_alive_cells = 0;  

		for (int i=x/size*rank; i<x/size*(rank+1); ++i)
		{
			for (int j=0; j<y; ++j)
			{
				for (int k=0; k<z; ++k)
				{
					int cell = grid_n0[i*y*z + j*z + k];
					int neighbors = 0;

					double time_bneighbor = MPI_Wtime ();   
 
					count_neighbors (neighbors, i, j, k);
 
					double time_fneighbor = MPI_Wtime ();   

					grid_n1[count*y*z + j*z + k] = life_estimation (neighbors, cell);
					local_alive_cells += grid_n1[count*y*z + j*z + k];  

					double time_uneighbor = MPI_Wtime ();               
				}
			}
			++count;
		}
	}

	/**
	-------- Function_3 in class which counts number of neighbors --------
	*/
    void count_neighbors (int &neighbors, int i, int j, int k)
	{
		for (int a = -1; a < 2; ++a)
		{
			for (int b = -1; b < 2; ++b)
			{
				for (int c = -1; c < 2; ++c)
				{
					if (!(a == 0 && b == 0 && c == 0))
					{
						// Boundary condition check 
						if ((i == 0 && a == -1) || (i == x-1 && a == 1))
							goto NEXT;
						else if ((j == 0 && b == -1) || (j == y-1 && b == 1))
							goto NEXT2;
						else if ((k == 0 && c == -1) || (k == z-1 && c == 1))
							goto NEXT3;
						else 
						{
							int l = i + a,
								m = j + b,
								n = k + c;
							neighbors += grid_n0[l*y*z + m*z + n];
						}
					}
					NEXT3: ;
				}
				NEXT2: ;
			}
			NEXT: ;
		}
	}

	/**
	-------- Function_4 in class which checks whether cell is alive or not -------- 
	*/
    int life_estimation (int neighbors, int cell)
	{
		// Game of life rules
		if (cell == 1 && neighbors >= El && neighbors <= Eu)     
			return 1;
		else if (cell == 0 && neighbors >= Fl && neighbors <= Fu)
			return 1;
		else
			return 0;
	}
}

// **** End of class cell file ****//
